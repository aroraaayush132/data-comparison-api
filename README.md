<!--
title: 'Data Comparison API'
description: 'This template demonstrates how to compare two excel files using the traditional Serverless Framework in Python.'
layout: Doc
framework: v2
platform: AWS
language: python2.7
authorLink: 'https://github.com/serverless'
authorName: 'Aayush Arora'
authorAvatar: 'https://github.com/aayusha761'
-->

This template demonstrates how to compare two excel files using the traditional Serverless Framework in Python.
# Serverless Framework Python REST API on AWS

This template does not include any kind of persistence (database). For a more advanced examples check out the [examples repo](https://github.com/serverless/examples/) which includes DynamoDB, Mongo, Fauna and other examples.

## Pre-Requisites

### Setup Serverless
Refer: https://www.serverless.com/plugins/serverless-offline

```
$ pip install openpyxl
$ pip install xlrd
$ pip install xlrd==1.2.0
```


## Lambdas

### fetch_column_names

Request Body:
```
{
    "excel1": "/Users/aayusharora/Desktop/files/sample-data-1.xlsx",
    "excel2": "/Users/aayusharora/Desktop/files/sample-data-2.xlsx"
}
```

Response Body:
```
{
    "status": "SUCCESS",
    "message": "Columns fetched successfully!!",
    "cols2": [
        "Payment ID",
        "Contract No",
        "ContractType",
        "Payment Type",
        "CURRENCY_CODE",
        "Unnamed: 5",
        "FX_Rate",
        "IS_PAYMENT_COMPLETE?",
        "Cost in USD",
        "Cost in Local",
        "Payment_due_date",
        "Approval_date",
        "Approver_Name",
        "Payment_Received_Date",
        "Cash_localAmt",
        "Cash_deducted_amt",
        "Cash_USDAmt",
        "Balance_Payment_Local",
        "Balance_Payment_USD",
        "Issue_Date",
        "Payment_Status",
        "Last_UpdatedDate",
        "ID",
        "Payment_Comments",
        "Products"
    ],
    "cols1": [
        "PAYMENT_ID",
        "CONTRACT_NO",
        "ContractType",
        "Payment Type",
        "CURRENCY_CODE",
        "FX_Rate",
        "Cost (USD)",
        "Cost (Local)",
        "Payment_due_date",
        "Approval_date",
        "Approver_Name",
        "Is_Payment_Done?",
        "Cash_localAmt",
        "Cash_deducted_amt",
        "Cash_USDAmt",
        "Balance_Payment_Local",
        "Balance_Payment_USD",
        "Payment_Received_Date",
        "Issue_Date",
        "Payment_Status",
        "Last_UpdatedDate",
        "Payment_Comments",
        "Products"
    ]
}
```

### compare_diff

Request Body:
```
{
    "files": {
        "excel1": "/Users/aayusharora/Desktop/files/sample-data-1.xlsx",
        "excel2": "/Users/aayusharora/Desktop/files/sample-data-2.xlsx"
    },
    "cols_map": {
        "Payment ID": "PAYMENT_ID",
        "Contract No": "CONTRACT_NO",
        "IS_PAYMENT_COMPLETE?": "Is_Payment_Done?",
        "Cost in USD": "Cost (USD)",
        "Cost in Local": "Cost (Local)"
    },
    "unique_key": ["PAYMENT_ID", "CONTRACT_NO", "ContractType", "Payment Type", "CURRENCY_CODE", "Payment_due_date", "Is_Payment_Done?", "Payment_Status"],
    "bool_cols": ["Is_Payment_Done?"]
}
```

Response Body:
```
{
    "status": "SUCCESS",
    "message": "Columns mapped successfully!!"
}
```

### Deploy Lambdas
```
$ serverless offline
```

## Usage

### Deployment

This example is made to work with the Serverless Framework dashboard which includes advanced features like CI/CD, monitoring, metrics, etc.

```
$ serverless login
$ serverless deploy
```

To deploy without the dashboard you will need to remove `org` and `app` fields from the `serverless.yml`, and you won’t have to run `sls login` before deploying.

After running deploy, you should see output similar to:

```bash
Serverless: Packaging service...
Serverless: Excluding development dependencies...
Serverless: Creating Stack...
Serverless: Checking Stack create progress...
........
Serverless: Stack create finished...
Serverless: Uploading CloudFormation file to S3...
Serverless: Uploading artifacts...
Serverless: Uploading service aws-python-rest-api.zip file to S3 (711.23 KB)...
Serverless: Validating template...
Serverless: Updating Stack...
Serverless: Checking Stack update progress...
.................................
Serverless: Stack update finished...
Service Information
service: aws-python-rest-api
stage: dev
region: us-east-1
stack: aws-python-rest-api-dev
resources: 12
api keys:
  None
endpoints:
  ANY - https://xxxxxxx.execute-api.us-east-1.amazonaws.com/dev/
functions:
  api: aws-python-rest-api-dev-hello
layers:
  None
```

_Note_: In current form, after deployment, your API is public and can be invoked by anyone. For production deployments, you might want to configure an authorizer. For details on how to do that, refer to [http event docs](https://www.serverless.com/framework/docs/providers/aws/events/apigateway/).

### Invocation

After successful deployment, you can call the created application via HTTP:

```bash
curl https://xxxxxxx.execute-api.us-east-1.amazonaws.com/dev/
```

Which should result in response similar to the following (removed `input` content for brevity):

```json
{
  "message": "Go Serverless v2.0! Your function executed successfully!",
  "input": {
  }
}
```

### Local development

You can invoke your function locally by using the following command:

```bash
serverless invoke local --function hello
```

Which should result in response similar to the following:

```
{
  "statusCode": 200,
  "body": "{\n  \"message\": \"Go Serverless v2.0! Your function executed successfully!\",\n  \"input\": \"\"\n}"
}
```

Alternatively, it is also possible to emulate API Gateway and Lambda locally by using `serverless-offline` plugin. In order to do that, execute the following command:

```bash
serverless plugin install -n serverless-offline
```

It will add the `serverless-offline` plugin to `devDependencies` in `package.json` file as well as will add it to `plugins` in `serverless.yml`.

After installation, you can start local emulation with:

```
serverless offline
```

To learn more about the capabilities of `serverless-offline`, please refer to its [GitHub repository](https://github.com/dherault/serverless-offline).

### Bundling dependencies

In case you would like to include 3rd party dependencies, you will need to use a plugin called `serverless-python-requirements`. You can set it up by running the following command:

```bash
serverless plugin install -n serverless-python-requirements
```

Running the above will automatically add `serverless-python-requirements` to `plugins` section in your `serverless.yml` file and add it as a `devDependency` to `package.json` file. The `package.json` file will be automatically created if it doesn't exist beforehand. Now you will be able to add your dependencies to `requirements.txt` file (`Pipfile` and `pyproject.toml` is also supported but requires additional configuration) and they will be automatically injected to Lambda package during build process. For more details about the plugin's configuration, please refer to [official documentation](https://github.com/UnitedIncome/serverless-python-requirements).
