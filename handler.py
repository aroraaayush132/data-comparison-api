import pandas as pd
import json
import numpy as np


def hello(event, context):
    body = {
        "message": "Go Serverless v2.0! Your function executed successfully!",
        "input": event,
    }

    response = {"statusCode": 200, "body": json.dumps(body)}

    return response


def fetch_column_names(event, context):
    body = json.loads(event['body'])
    excel1 = body['excel1']
    excel2 = body['excel2']

    # Read in the two files but call the data old and new and create columns to track
    # old = pd.read_csv(excel1)
    old = pd.read_excel(excel1, 'Sheet1', na_values=['NA'], encoding='utf-8')
    new = pd.read_excel(excel2, 'Sheet1', na_values=['NA'])

    # Fetch columns of source and target excels
    old_cols = list(old.columns)
    new_cols = list(new.columns)

    return {
        'statusCode': 200,
        'body': json.dumps({
            "cols1": old_cols,
            "cols2": new_cols,
            "status": "SUCCESS",
            "message": "Columns fetched successfully!!"
        })
    }


def compare_diff(event, context):
    # Fetch column mapping from body
    body = json.loads(event['body'])

    cols_map = body['cols_map']
    unique_key = body['unique_key']
    bool_cols = body['bool_cols']

    # Fetch path of target file from parameters
    files = json.loads(json.dumps(body['files']))
    excel1 = files['excel1']
    excel2 = files['excel2']

    # Read in the two files but call the data old and new and create columns to track
    old = pd.read_excel(excel1, 'Sheet1', na_values=['NA'])
    new = pd.read_excel(excel2, 'Sheet1', na_values=['NA'])

    print(old)
    print(new)

    # map column names
    new.rename(columns=cols_map, inplace=True)

    # update target file columns and sequencing of target columns
    new = new.reindex(columns=list(old.columns))

    # Add extra column 'version' for identifying two excels
    old['version'] = "old"
    new['version'] = "new"

    print(old)
    print(new)

    # Join all the data together and ignore indexes so it all gets added
    full_set = pd.concat([old, new], ignore_index=True)

    # Update YES to 1 and NO to 0
    for cols in bool_cols:
        full_set[cols] = full_set[cols].map({'YES': '1', 'NO': '0', 1: '1', 0: '0'})

    # Drop all duplicate columns
    changes = full_set.drop_duplicates(subset=list(old.columns), keep='last')

    # print changes

    # We want to know where the duplicate columns are, that means there have been changes
    dupe_accts = changes.set_index(list(unique_key)).index.get_duplicates()
    dupe_accts_frames = dupe_accts.to_frame().reset_index(drop=True)
    # print dupe_accts_frames

    # Calling merge() function
    dupes = pd.merge(dupe_accts_frames, changes, how='inner')
    # print(dupes)

    change_new = dupes[(dupes["version"] == "new")]
    change_old = dupes[(dupes["version"] == "old")]

    # Drop the temp columns - we don't need them now
    change_new = change_new.drop(['version'], axis=1)
    change_old = change_old.drop(['version'], axis=1)

    # print change_old
    # print change_new

    # Index on the tables
    change_new.set_index(unique_key, inplace=True)
    change_old.set_index(unique_key, inplace=True)

    # print change_old
    # print change_new

    # Now we can diff because we have two data sets of the same size with the same index
    try:
        diff_panel = pd.Panel(dict(df1=change_old, df2=change_new))
        diff_output = diff_panel.apply(report_diff, axis=0)

        # print type(diff_output)

        writer = pd.ExcelWriter("my-diff-2.xlsx", engine='xlsxwriter')
        diff_output.to_excel(writer, "changed")

        # get xlswriter objects
        workbook = writer.book
        worksheet = writer.sheets['changed']
        worksheet.hide_gridlines(2)
        worksheet.set_default_row(15)

        # get number of rows of the df diff
        row_count_str = str(len(diff_output.index) + 1)

        # define and apply formats
        highligt_fmt = workbook.add_format({'font_color': '#FF0000', 'bg_color': '#B1B3B3'})
        worksheet.conditional_format('A1:ZZ' + row_count_str,
                                     {'type': 'text', 'criteria': 'containing', 'value': '--->',
                                      'format': highligt_fmt})
        writer.save()
    except Exception:
        print("No Unique Key matches this criteria.")

    return {
        'statusCode': 200,
        'body': json.dumps({
            "status": "SUCCESS",
            "message": "Excel generated successfully!!"
        })
    }


def compare_diff_pivot(event, context):
    # Fetch column mapping from body
    body = json.loads(event['body'])

    cols_map = body['cols_map']
    index = body['index']
    bool_cols = body['bool_cols']

    # Fetch path of target file from parameters
    files = json.loads(json.dumps(body['files']))
    excel1 = files['excel1']
    excel2 = files['excel2']

    # Read in the two files but call the data old and new and create columns to track
    old = pd.read_excel(excel1, 'Sheet1', na_values=['NA'])
    new = pd.read_excel(excel2, 'Sheet1', na_values=['NA'])

    # print(old)
    # print(new)

    # map column names
    new.rename(columns=cols_map, inplace=True)

    # update target file columns and sequencing of target columns
    new = new.reindex(columns=list(old.columns))

    # Add extra column 'version' for identifying two excels
    old['Source'] = "SQL"
    new['Source'] = "Snowflake"

    # print(old)
    # print(new)

    # Join all the data together and ignore indexes so it all gets added
    full_set = pd.concat([old, new], ignore_index=True)

    # Update YES to 1 and NO to 0
    for cols in bool_cols:
        full_set[cols] = full_set[cols].map({'YES': '1', 'NO': '0', 1: '1', 0: '0'})

    # Drop all duplicate columns
    # changes = full_set.drop_duplicates(subset=list(old.columns), keep='last')

    values = set(old.columns) - set(index)

    try:
        pivot = full_set.fillna("NULL").pivot_table(
            index=index, columns=['Source'],
            values=values,
            aggfunc=lambda x: x, observed=True)

        print(pivot)

        writer = pd.ExcelWriter("pivot1.xlsx", engine='xlsxwriter')
        pivot.to_excel(writer, "changed")
        writer.save()
    except Exception:
        print("No index matches this criteria.")

    return {
        'statusCode': 200,
        'body': json.dumps({
            "status": "SUCCESS",
            "message": "Excel generated successfully!!"
        })
    }


# Define the diff function to show the changes in each field
def report_diff(x):
    return x[0] if x[0] == x[1] else '{} ---> {}'.format(*x)
